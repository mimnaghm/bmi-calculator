//
//  BMI.swift
//  BMI Calculator
//
//  Created by Marcus Mimnagh on 8/4/20.
//  Copyright © 2020 Marcus Mimnagh. All rights reserved.
//

import UIKit

struct BMI {
    let value: Float
    let advice: String
    let color: UIColor
    
}
