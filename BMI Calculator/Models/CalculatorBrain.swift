//
//  CalculatorBrain.swift
//  BMI Calculator
//
//  Created by Marcus Mimnagh on 8/4/20.
//  Copyright © 2020 Marcus Mimnagh. All rights reserved.
//

import UIKit

struct CalculatorBrain {
    
    var bmi: BMI?
    
    func getBMIValue() -> String {
        let bmiTo1DecimalPlace = String(format: "%.1f", bmi?.value ?? 0.0)
        return bmiTo1DecimalPlace
    }
    
    mutating func calculateBMI(height: Float, weight: Float) {
        
        let bmiValue = weight / pow(height, 2) * 703
        
        if bmiValue < 18.5 {
            bmi = BMI(value: bmiValue, advice: "Eat more fries", color: #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1))
        } else if bmiValue < 24.9 {
            bmi = BMI(value: bmiValue, advice: "Looking good", color: #colorLiteral(red: 0.2215491945, green: 0.9884998834, blue: 0.0929272359, alpha: 1))
        } else {
            bmi = BMI(value: bmiValue, advice: "Hit the treadmill", color: #colorLiteral(red: 0.8541423741, green: 0.1274018562, blue: 0, alpha: 1))
        }
    }
    
    func getAdvice() -> String {
        let advice = bmi?.advice
        return advice!
    }
    
    func getColor() -> UIColor {
        let color = bmi?.color
        return color!
    }
}

